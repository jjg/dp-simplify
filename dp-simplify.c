/*
  dp-simplify.c

  Douglas-Peucker polyline simplification
  J.J.Green 2003
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#include "options.h"

typedef struct vertex_t
{
  double x, y;
} vertex_t;

typedef struct opt_t
{
  int    verbose;
  int    minvert;
  char   sepchar;
  double tol;
  struct
  {
    char *file;
    FILE *stream;
  } in, out;
} opt_t;

static int process_out(opt_t*);
static int process_in(opt_t*);
static int process(opt_t*);

int main(int argc, char *argv[])
{
  struct gengetopt_args_info info;
  opt_t opt;
  int   n, err;

  if (options(argc, argv, &info) != 0)
    {
      fprintf(stderr, "failed to read options\n");
      return EXIT_FAILURE;
    }

  if (info.help_given)    options_print_help();
  if (info.version_given) options_print_version();

  opt.out.file = (info.output_given ? info.output_arg : NULL);
  opt.verbose  = info.verbose_given;
  opt.tol      = info.tol_arg;
  opt.minvert  = (info.minvert_given ? info.minvert_arg : 0);
  opt.sepchar  = (info.sepchar_given ? (info.sepchar_arg[0]) : '#');

  opt.in.stream = opt.out.stream = NULL;

  n = info.inputs_num;

  if (opt.verbose)
    printf("This is %s (version %s)\n", OPTIONS_PACKAGE, OPTIONS_VERSION);

  switch (n)
    {
    case 0:
      opt.in.file = NULL;
      err = process_out(&opt);
      break;
    case 1:
      opt.in.file = info.inputs[0];
      err = process_out(&opt);
      break;
    default:
      options_print_help();
      err = 1;
    } 

  if (err)
    fprintf(stderr, "error!\n");
  else
    if (opt.verbose) printf("done.\n");
   
  return (err ? EXIT_FAILURE : EXIT_SUCCESS);
}

static int process_out(opt_t* opt)
{
  int err = 1;

  if ((opt->verbose) && (!opt->out.file))
    fprintf(stderr, "must specify output file with -v\n");
  else
    {
      if (opt->out.file)
	{
	  if ((opt->out.stream = fopen(opt->out.file, "w")) != NULL)
	    {
	      err = process_in(opt);
	      fclose(opt->out.stream);
	    }
	  else
	    fprintf(stderr, "failed to open %s : %s\n", 
		    opt->out.file, strerror(errno));
	}
      else
	{
	  opt->out.stream = stdout;
	  err = process_in(opt);
	}
    }

  return err;
}

static int process_in(opt_t* opt)
{
  int err = 1;

  if (opt->in.file)
    {
      if ((opt->in.stream = fopen(opt->in.file, "r")) != NULL)
	{
	  err = process(opt);
	  fclose(opt->in.stream);
	}
      else
	fprintf(stderr, "failed to open %s : %s\n", 
		opt->in.file, strerror(errno));
    }
  else
    {
      opt->in.stream = stdin;
      err = process(opt);
    }

  return err;
}

#define STRDEF(s, def) ((s) ? (s) : (def)) 

#define BUFSZ 512

static int process_block(opt_t*, int, vertex_t*);

static int process(opt_t* opt)
{
  char      buf[BUFSZ];
  double    x, y;
  int       n=0, m=512, err=0;
  vertex_t *v;

  if (opt->verbose)
    {
      printf("reading\n  %s\n", STRDEF(opt->in.file , "<stdin>"));
      printf("writing\n  %s\n", STRDEF(opt->out.file, "<stdout>"));
    }

  if (opt->verbose)
    printf("%8s%8s%8s\n", "original", "approx", "comp %");

  if ((v = malloc(m*sizeof(vertex_t))) == NULL)
    return 1;

  while (fgets(buf, BUFSZ, opt->in.stream))
    {
      if (buf[0] == opt->sepchar)
	{
	  err |= process_block(opt, n, v);
	  n = 0;
	  continue;
	}

      if (sscanf(buf, "%lf %lf", &x, &y) != 2) continue;

      v[n].x = x; 
      v[n].y = y;

      n++;

      if (n >= m)
	{
	  m *= 2;

	  if ((v = realloc(v, m*sizeof(vertex_t))) == NULL)
	    return 1;
	}
    }

  err |= process_block(opt, n, v);

  free(v);

  return err;
}

static int poly_simplify(double, vertex_t*, int, vertex_t*, int*);

static int process_block(opt_t *opt, int n, vertex_t *v)
{
  vertex_t   *vs;
  int         err, ns=0, i, drop;
  static int  wrtn=0;

  if (!n) return 0;

  if ((vs = malloc(n*sizeof(vertex_t))) == NULL)
    return 1;

  err = poly_simplify(opt->tol, v, n, vs, &ns);  

  drop = ((opt->minvert) && (ns < opt->minvert));

  if (! drop)
    {
      /* 
	 write record separator before the 2nd and 
	 subsequent records
      */ 

      if (wrtn) fprintf(opt->out.stream, "%c\n", opt->sepchar);
      else wrtn++;

      for (i=0 ; i<ns ; i++)
	fprintf(opt->out.stream, "%f\t%f\n", vs[i].x, vs[i].y);
    }

  if (opt->verbose)
    printf("%8.i%8.i%8.2f %s\n", 
	   n, 
	   ns, 
	   100*(double)ns/(double)n, 
	   (drop ? "(dropped)" : "")
	   );

  free(vs);

  return err;
}

/*
  following block reused from dp.cc, a simple recursive
  implementation by softsurfer -- cheers mate!

  Ive made some cosmetic changes, errors intoduced are
  probably my own

  jjg 2004

  original notice follows

  ----
  Copyright 2002, softSurfer (www.softsurfer.com)
  This code may be freely used and modified for any purpose
  providing that this copyright notice is included with it.
  SoftSurfer makes no warranty for this code, and cannot be held
  liable for any real or imagined damage resulting from its use.
  Users of this code must verify correctness for their application.
  ---
*/

/*
  simple vector functions

  these would be best implemented as calls to appropriate
  functions in a library (the GSL say), but we need so little
  it seems a shame to intoduce the dependency
*/

static double dot(vertex_t u, vertex_t v)
{
  return u.x*v.x + u.y*v.y;
}

static double norm2(vertex_t u)
{
  return dot(u, u);
}

static vertex_t vdiff(vertex_t u, vertex_t v)
{
  vertex_t w;

  w.x = u.x - v.x;
  w.y = u.y - v.y;

  return w;
}

static vertex_t vadd(vertex_t u, vertex_t v)
{
  vertex_t w;

  w.x = u.x + v.x;
  w.y = u.y + v.y;

  return w;
}

static vertex_t vsm(double l, vertex_t v)
{
  vertex_t w;

  w.x = l*v.x;
  w.y = l*v.y;

  return w;
}

static double d2(vertex_t u, vertex_t v)
{
  return norm2(vdiff(u, v));
}

/*
  poly_simplify():
    Input:  tol = approximation tolerance
            p   = polyline array of vertex points
    Output: q   = simplified polyline vertices
*/

static int simplify_dp(double, vertex_t*, int, int, int*);

static int poly_simplify(double t, vertex_t* pv, int n, vertex_t* qv, int* pm)
{
  int       i, k, m, l;
  int       mk[n];
  double    t2 = t*t;
  vertex_t  v[n];

  /* clear errno to chck for math errors */

  errno = 0;

  /* vertex reduction within tolerance of prior vertex cluster */

  v[0] = pv[0];
  
  for (i=k=1, l=0 ; i<n ; i++) 
    {
      if (d2(pv[i], pv[l]) < t2) continue;
      v[k++] = pv[i];
      l = i;
    }

  if (l < n-1) v[k++] = pv[n-1];
  
  /* Douglas-Peucker polyline simplification */

  for (i=1 ; i<k-1 ; i++) 
    mk[i] = 0;

  mk[0]   = 1;
  mk[k-1] = 1;

  if (simplify_dp(t2, v, 0, k-1, mk) != 0) return 1;
  
  /* copy marked vertices to the simplified polyline */

  for (i=m=0; i<k; i++) 
    {
      if (mk[i]) qv[m++] = v[i];
    }

  *pm = m;

  /* check for math errors */

  if (errno)
    {
      fprintf(stderr, "error in simplification : %s\n", strerror(errno));
      return 1;
    }

  return 0;
}

/*
  simplify_dp():
  
  This is the Douglas-Peucker recursive simplification routine
  It just marks vertices that are part of the simplified polyline
  for approximating the polyline subchain v[j] to v[k].

    Input:  t2   = tolerance squared
            v[]  = polyline array of vertex points
            j, k  = indices for the subchain v[j] to v[k]
    Output: mk[] = array of markers matching vertex array v[]
*/

static int simplify_dp(double t2, vertex_t* v, int j, int k, int* mk)
{
  double   maxd2 = 0;		     /* distance squared of farthest vertex */
  int      maxi  = j;		     /* index of vertex farthest from S */
  vertex_t u     = vdiff(v[k], v[j]); /* segment direction vector */
  double   cu    = dot(u, u);         /* segment length squared */
  int      i;

  /*
    test each vertex v[i] for max distance from S
    compute using the Feb 2001 Algorithm's dist_Point_to_Segment()
    Note: this works in any dimension (2D, 3D, ...)
  */
    
  vertex_t  w, p;
  double    b, cw, dv2;
 
  if (k <= j+1) return 0;

  for (i=j+1 ; i<k ; i++)
    {
      /* compute distance squared */

      w  = vdiff(v[i], v[j]);
      cw = dot(w, u);

      if (cw <= 0)
	dv2 = d2(v[i], v[j]);
      else if (cu <= cw)
	dv2 = d2(v[i], v[k]);
      else 
	{
	  b   = cw/cu;
	  p   = vadd(v[j], vsm(b, u));
	  dv2 = d2(v[i], p);
        }

      /* test with current max distance squared */
      
      if (dv2 <= maxd2) continue;

      /* v[i] is a new max vertex */

      maxi  = i;
      maxd2 = dv2;
    }

  /* 
     error is worse than the tolerance 
     split the polyline at the farthest vertex from S 
  */

  if (maxd2 > t2)
    {
      int err = 0;

      /* mark v[maxi] for the simplified polyline */
      
      mk[maxi] = 1;

      /* recursively simplify the two subpolylines at v[maxi] */
      
      err |= simplify_dp(t2, v, j, maxi, mk);  /* polyline v[j] to v[maxi] */
      err |= simplify_dp(t2, v, maxi, k, mk);  /* polyline v[maxi] to v[k] */

      if (err) return 1;
    }

  /* else the approximation is OK, so ignore intermediate vertices */

  return 0;
}

 


