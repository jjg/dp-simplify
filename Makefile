# Makefile for dp-simplify
#
# J.J.Green 2003

INSTDIR  = /usr/local

CC       = gcc
CFLAGS  += -Wall -O3
LDFLAGS += 
LDLIBS  += -lm

RUBBISH += *~ dp-simplify $(OBJ)

all : dp-simplify

check test : show

install : dp-simplify
	install -m755 dp-simplify $(INSTDIR)/bin/dp-simplify
	install -m644 dp-simplify.1 $(INSTDIR)/man/man1/dp-simplify.1

clean :
	$(RM) $(RUBBISH)

.PHONY : all check test show install clean

# This is data from the UK Ordinance Survey (codeline),
# used for testing purposes.  It is not distributed with
# the package

POLY     = /usr/local/share/data/os/codeline/s/3.osgb
RUBBISH += test.xy test.eps

show : test.eps
	gv test.eps

test.eps : test.sh test.xy
	sh test.sh $(POLY) test.xy test.eps

test.xy : dp-simplify Makefile
	./dp-simplify -t100 -m10 -v -o test.xy $(POLY)

OBJ      = options.o dp-simplify.o
RUBBISH += dp-simplify $(OBJ)

options.c options.h : options.ggo
	gengetopt -u -i options.ggo -f options -F options

dp-simplify.o : dp-simplify.c options.h 

dp-simplify : $(OBJ)

MANXSL = http://docbook.sourceforge.net/release/xsl/current/manpages/docbook.xsl

dp-simplify.1 : dp-simplify.xml
	xsltproc $(MANXSL) dp-simplify.xml
