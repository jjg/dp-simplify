#! /bin/sh
#
# test plot 

ORIG=$1
APPROX=$2
PS=$3

PRJ="-Jx0.002"
RNG="-R433000/436500/386000/390000"

COMMON="+defaults $RNG $PRJ"

ACOL=220/0/0
OCOL=0

APEN=6/$ACOL
OPEN=3/$OCOL

GMT psxy $APPROX $COMMON -L -W$APEN -M\# -K     > $PS 
GMT psxy $ORIG   $COMMON -L -W$OPEN -M\# -K -O >> $PS 
GMT psbasemap    $COMMON -B1000             -O >> $PS
